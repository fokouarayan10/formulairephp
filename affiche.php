//fichier pour affichier dans les elements de la base de donnees

<!DOCTYPE html>
<html>

<head>

    <title>affiche </title>
    <link rel="stylesheet" href="./bootstrap.min.css">

</head>

<body>

    <h3> Listes de matieres</h3>
    
    <div class="container">
       <?php 
        echo '<table class ="table">';
            echo '<thead">';
                echo "<tr>";
                    echo "<th> Numero</th>";
                    echo "<th> nom</th>";
                    echo "<th> univeriste</th>";
                echo "</tr>";
                
                class TableRows extends RecursiveIteratorIterator
                {
                    function __constructor ($it)
                    {
                        parent:: __constructor($it, self::LEAVES_ONLY);
                    }

                    function current ()
                    {
                        return "<td>" .parent::current(). "</td>";
                    }

                    function beginChildren()
                    {
                        echo "<tr>";
                    }

                    function endChildren()
                    {
                        echo "</tr>" . "\n";
                    }
                }

                $serveur = "localhost";
                $util = "root";
                $mdp = "";
                $bd = "bdathletes";

                try
                {
                    $connection = new PDO("mysql:host=$serveur; dbname=$bd", $util, $mdp);
                    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
                    $stmt = $connection->prepare("SELECT * FROM athletes");
                    $stmt->execute();

                    $resultat = $stmt->setFetchMode(PDO::FETCH_ASSOC);
                    foreach(new TableRows(new RecursiveArrayIterator($stmt->fetchAll())) as $k=>$v)
                    {
                        echo $v;
                    }
                }
                
                catch(PDOException $e)
                {
                    echo "Erreur: ".$e->getMessage();
                }

                $connection = null;

            echo "</thead>";
        echo "</table>";    
    ?>
    </div>

</body>

</html>